###########################################################################
FEniCS'14 at The Center in Paris, University of Chicago, 16 -- 18 June 2014
###########################################################################

| Featured article 2014-10-11

We are pleased to announce the 2014 edition of the annual FEniCS Workshop:

  *FEniCS'14: Expressive and high performance scientific computing*

which will take place 16 -- 17 June 2014 at `The Center in Paris,
University of Chicago <http://centerinparis.uchicago.edu/>`__, Paris,
France. All FEniCS users, developers and other interested parties are
invited to discuss current and future directions in scientific
computing, approaches to scientific software development and FEniCS
development.

Please mark your calendars and see `our Lanyrd page
<http://lanyrd.com/2014/fenics14>`__ as more details become available.
