.. _releases:

########################
Release notes for FEniCS
########################
.. toctree::
    :maxdepth: 1
    :glob:

    1.0-beta/*
    1.1/*
